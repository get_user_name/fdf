/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 15:52:19 by gkoch             #+#    #+#             */
/*   Updated: 2019/01/24 12:42:42 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include	"./libft.h"
# include	"../mlx/mlx.h"
# include	<unistd.h>
# include	<fcntl.h>
# include	<math.h>
# include	<stdlib.h>

typedef	struct		s_root
{
	int				width;
	int				height;
	void			*mlx_ptr;
	void			*win_ptr;
	int				zoom;
	int				x;
	int				y;
	int				x_axis;
	int				y_axis;
	int				z_axis;
	struct	s_map	*map;
	struct	s_mtrx	*mtrx;

}					t_root;

typedef	struct		s_map
{
	struct	s_line	*line;
	struct	s_map	*next;
}					t_map;

typedef	struct		s_line
{
	int				number;
	int				color;
	struct	s_line	*next;
}					t_line;

typedef	struct		s_strip
{
	int				*zxy;
	int				color;
	struct	s_strip	*next;
}					t_strip;

typedef	struct		s_mtrx
{
	int				x1;
	int				x2;
	int				y1;
	int				y2;
	struct	s_strip	*strip;
	struct	s_mtrx	*next;
}					t_mtrx;

typedef	struct		s_bzm
{
	int				deltaX;
	int 			deltaY;
	int				signX;
	int				signY;
	int				error;
}					t_bzm;

int			push_row(t_map **begin_list, t_map *data);
int			push_line(t_line **begin_list, t_line *data);
int			push_strip(t_strip **begin_list, t_strip *data);
int			push_mtrx(t_mtrx **begin_list, t_mtrx *data);

t_root		*create_root(int width, int height, t_map *map, int zoom);
t_map		*create_row(t_line *line);
t_line		*create_line(int number);
t_strip		*create_strip();
t_mtrx		*create_mtrx(t_strip *strip);
t_bzm		*create_bzm(t_strip *strip1, t_strip *strip2);

void		ft_draw(t_root *root);
t_mtrx		*get_mtrx(t_map *map, t_root *root);
void		paint(t_root *root, t_mtrx *mtrx);
void		run_points(t_root *root);

int			deal_key(int key, t_root *root);

t_map		*ft_read(char *str);

#endif
