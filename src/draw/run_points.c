/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   run_points.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 20:48:29 by gkoch             #+#    #+#             */
/*   Updated: 2019/01/24 12:06:42 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/fdf.h"

static	void	set_points(t_mtrx **mtrx, t_strip *strip)
{
	if ((*mtrx)->x1 > (strip->zxy)[1])
		(*mtrx)->x1 = (strip->zxy)[1];
	if ((*mtrx)->x2 < (strip->zxy)[1])
		(*mtrx)->x2 = (strip->zxy)[1];
	if ((*mtrx)->y1 > (strip->zxy)[2])
		(*mtrx)->y1 = (strip->zxy)[2];
	if ((*mtrx)->y2 < (strip->zxy)[2])
		(*mtrx)->y2 = (strip->zxy)[2];
}

static	void	valid_points(t_root *root)
{
	int		x;
	int		y;

	x = (root->width - ft_abs(root->mtrx->x1) - root->mtrx->x2) / 2;
	if (root->mtrx->x1 < 0)
		x += ft_abs(root->mtrx->x1);
	y = (root->height - ft_abs(root->mtrx->y1) - root->mtrx->y2) / 2;
	if (root->mtrx->y1 < 0)
		y += ft_abs(root->mtrx->y1);
	root->mtrx->x1 = x;
	root->mtrx->y1 = y;
}

void			run_points(t_root *root)
{
	t_mtrx	*value;
	t_strip *strip;

	value = root->mtrx;
	while (value)
	{
		strip = value->strip;
		while (strip)
		{
			set_points(&(root->mtrx), strip);
			strip = strip->next;
		}
		value = value->next;
	}
	valid_points(root);
}
