/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mtrx.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 20:39:26 by gkoch             #+#    #+#             */
/*   Updated: 2019/01/24 16:20:27 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/fdf.h"

void			iso(t_strip *strip, t_root *root)
{
	int		x;
	int		y;
	int		z;
	double	pi;

	pi = 3.1415;
	x = (strip->zxy)[1];
	y = (strip->zxy)[2];
	z = (strip->zxy)[0];
	(strip->zxy)[2] = (int)(y * cos(root->x_axis * pi / 180)
		+ z * sin(root->x_axis * pi / 180));
	(strip->zxy)[0] = (int)(-y * sin(root->x_axis * pi / 180)
		+ z * cos(root->x_axis * pi / 180));
	y = (strip->zxy)[2];
	z = (strip->zxy)[0];
	(strip->zxy)[1] = (int)(x * cos(root->y_axis * pi / 180)
		+ z * sin(root->y_axis * pi / 180));
	(strip->zxy)[0] = (int)(z * cos(root->y_axis * pi / 180)
		- x * sin(root->y_axis * pi / 180));
	x = (strip->zxy)[1];
	z = (strip->zxy)[0];
	(strip->zxy)[1] = (int)(x * cos(root->z_axis * pi / 180)
		- y * sin(root->z_axis * pi / 180));
	(strip->zxy)[2] = (int)(x * sin(root->z_axis * pi / 180)
		+ y * cos(root->z_axis * pi / 180));
}

static	t_strip	*ft_set_strip(int x, int y, t_line *line, t_root *root)
{
	t_strip	*strip;

	strip = create_strip();
	(strip->zxy)[0] = line->number * root->zoom / 8;
	(strip->zxy)[1] = x * root->zoom;
	(strip->zxy)[2] = y * root->zoom;
	strip->color = line->color;
	iso(strip, root);
	return (strip);
}

static	t_strip	*get_strips(int y, t_line *line, t_root *root)
{
	t_strip *strip;
	int		x;

	x = 0;
	strip = NULL;
	while (line)
	{
		push_strip(&strip, ft_set_strip(x, y, line, root));
		line = line->next;
		x++;
	}
	return (strip);
}

t_mtrx			*get_mtrx(t_map *map, t_root *root)
{
	t_mtrx	*mtrx;
	int		y;

	y = 0;
	mtrx = NULL;
	while (map)
	{
		push_mtrx(&mtrx, create_mtrx(get_strips(y, map->line, root)));
		map = map->next;
		y++;
	}
	return (mtrx);
}
