/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 20:38:09 by gkoch             #+#    #+#             */
/*   Updated: 2019/01/24 12:42:58 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/fdf.h"

void	ft_draw(t_root *root)
{
	root->mtrx = get_mtrx(root->map, root);
	run_points(root);
	paint(root, root->mtrx);
}
