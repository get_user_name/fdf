/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   control.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 19:11:50 by gkoch             #+#    #+#             */
/*   Updated: 2019/01/24 12:42:25 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/fdf.h"

int	deal_key(int key, t_root *root)
{
	mlx_clear_window(root->mlx_ptr, root->win_ptr);
	if (key == 53)
		exit(EXIT_FAILURE);
	root->zoom += (key == 24 ? 1 : 0);
	root->zoom += (key == 27 ? -1 : 0);
	root->y += (key == 125 ? ft_abs(root->zoom) : 0);
	root->y += (key == 126 ? -ft_abs(root->zoom) : 0);
	root->x += (key == 124 ? ft_abs(root->zoom) : 0);
	root->x += (key == 123 ? -ft_abs(root->zoom) : 0);
	root->x_axis += (key == 1 ? 2 : 0);
	root->y_axis += (key == 2 ? 2 : 0);
	root->z_axis += (key == 14 ? 2 : 0);
	root->x_axis += (key == 13 ? -2 : 0);
	root->y_axis += (key == 0 ? -2 : 0);
	root->z_axis += (key == 12 ? -2 : 0);
	if (key == 15)
	{
		root->x_axis = 0;
		root->y_axis = 0;
		root->z_axis = 0;
		root->y = 0;
		root->x = 0;
	}
	ft_draw(root);
	return (1);
}
