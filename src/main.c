#include "../includes/fdf.h"

void	start_win(char **str, int ac, t_root **root)
{
	(*root) = create_root(1024, 1024, ft_read(str[1]), 1);
	(*root)->width = (ac >= 3) ? ft_atoi(str[2]): 1024;
	(*root)->height = (ac >= 4) ? ft_atoi(str[3]): 1024;
	(*root)->zoom = (ac >= 5) ? ft_atoi(str[4]): 1;
	(*root)->mlx_ptr = mlx_init();
	(*root)->win_ptr = mlx_new_window((*root)->mlx_ptr, (*root)->width, (*root)->height, str[1]);
}

void	fdf(char **str, int ac)
{
	t_root	*root;

	start_win(str, ac, &root);
	ft_draw(root);
	mlx_hook(root->win_ptr, 2, 0, deal_key, root);
	mlx_loop(root->mlx_ptr);
}

int	 main(int ac, char **av)
{
	int		n;

	char	*test;
	n = 0;
	if (ac > 1)
		fdf(av, ac);
	else
		ft_putendl("There is no argument.");
	return (0);
}
