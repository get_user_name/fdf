# **************************************************************************** #
#																										#
#																			:::		::::::::	 #
#	 Makefile														 :+:		:+:	 :+:	 #
#																	  +:+ +:+			+:+		#
#	 By: gkoch <gkoch@student.42.fr>					 +#+  +:+		 +#+			#
#																 +#+#+#+#+#+	+#+				#
#	 Created: 2019/01/17 15:32:38 by gkoch				 #+#	 #+#				  #
#	 Updated: 2019/01/17 15:49:23 by gkoch				###	########.fr		  #
#																										#
# **************************************************************************** #

NAME = fdf
SRC = ./src/main.c \
		./src/struct/create.c ./src/struct/create2.c ./src/struct/push.c \
		./src/draw/draw.c ./src/draw/mtrx.c ./src/draw/run_points.c ./src/draw/print.c \
		./src/read/read.c \
		./src/control/control.c
OBJ=$(SRC:.c=.o)
CC = clang
CFLAGS = -g #-Wall -Wextra -Werror
LIB = libft/libft.a
LIBDIR = libft/
LIBSPATH = -I libft/ -I /usr/local/include/
HDR = fdf.h
LINK = -lmlx -framework OpenGL -framework AppKit -L /usr/local/lib/

all: lib $(OBJ) $(NAME)

$(NAME): $(LIB) $(OBJ)
	$(CC) $(CFLAGS) $(LIBSPATH) -o $(NAME) $(LINK) $(LIB) $(OBJ)

lib:
	make -C $(LIBDIR)

%.o: %.c $(HDR) $(LIB)
	$(CC) $(CFLAGS) -c $< -o $@ $(LINK)

fclean: clean
	/bin/rm -f $(NAME)
	make -C $(LIBDIR) fclean

clean: cleanlib
	/bin/rm -f $(OBJ)

cleanlib:
	make -C $(LIBDIR) clean

re: fclean all